import axios from 'axios'

export default {

  state: {
      cart: [],
  },
  mutations: {
    arrayCart : (state, payload) => {
      state.cart = payload
    },
    addToCart: (state, product) => {
        let founded=false;
        if (state.cart != null) {
            state.cart.forEach(function(item,index) {
                if (product.uuid == item.uuid) {
                    founded=true;
                    item.count += product.temp_count;
                }
            });
        }
        if (!founded) {
            product.count = product.temp_count;
            state.cart.push(product);
        }
    },

    setProductCartCount: (state, payload) => {
        if (state.cart != null) {
            state.cart.forEach(function(item,index) {
                if (payload.product.uuid == item.uuid) {
                    item.count = payload.value;
                }
            });
        }
    },


  },

//  actions: {
//    async getBrands( {commit} ) {
//        return  await new Promise((resolve) => {
//            axios.get("https://test2.iti.dp.ua/api/brand/")
//                .then(response => {
//            commit('arrayBrands', response.data);
//                resolve()
//            });
//        })
//    },
//  },
  getters: {
    getCart(state) {
       return state.cart;
    },
    getProductsCount(state) {
        return (state.cart!=null)?state.cart.length:0;
    },
    getProductTotalSum: (state) => (product) => {
        let result;
        state.cart.forEach(function(item,index) {
            if (product.uuid == item.uuid) {
                result=item.count*item.price;
                return;
            }
        });
        return result;
    },
    getCartTotalSum: (state) => () => {
        let result=0;
        state.cart.forEach(function(item,index) {
            result += item.count*item.price;
        });
        return result;
    },
    getSumProductsCount(state){
      let sum = 0;
      state.cart.forEach(function(item,index) {
        sum+=item.count;
      });
      return sum;
    },
    getProductCartCount: (state) => (product) => {
        let result;
        state.cart.forEach(function(item,index) {
            if (product.uuid == item.uuid) {
                result=item.count;
                return;
            }
        });
        return result;
    },

  }
}
