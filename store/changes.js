import axios from 'axios'
const debug = require('debug')('app:demo')

export default {

  state: {
    current_version: null,
  },
  mutations: {
    arrayCurrentVersion : (state, payload) => {
      //debug(payload);
      state.current_version = payload;
    },
  },
  actions: {
    async getLastChanges( {commit, dispatch} ) {
        return  await new Promise((resolve) => {
            axios.get("https://test2.iti.dp.ua/api/changes/")
                .then(response => {
                commit('arrayCurrentVersion', response.data);
                resolve()
            });
        })
    }
  },
  getters: {
    getCurrentVersion(state) {
       return state.current_version;
    },
  }
}
