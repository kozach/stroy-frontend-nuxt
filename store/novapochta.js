import axios from 'axios'
const debug =require('debug')('app:demo')

export default {

  state: {
    np_apikey: 'aba23a3720e063f4a38bbb811f874cc7',
    np_areas: [],
    np_settlements: [],
  },
  mutations: {
    arrayAreaInformation : (state, payload) => {
        state.np_areas = [];
        let areas = []
        payload["data"].forEach(function(item,index) {
            let obj = {};
            if (areas.indexOf(item["AreaDescriptionRu"]) == -1) {
                areas.push(item["AreaDescriptionRu"]);
                obj["key"] = item["Area"];
                obj["value"] = item["AreaDescriptionRu"];
                state.np_areas.push(obj);
            }
        })
        state.np_settlements = payload["data"];
        //alert(state.np_settlements);
    },
  },
  actions: {
    async getNPAreaInformation( {commit, state, dispatch} ) {
        return  await new Promise((resolve) => {
            axios.post('https://api.novaposhta.ua/v2.0/json/', { 'apiKey':  'aba23a3720e063f4a38bbb811f874cc7', 'modelName': 'Address', 'calledMethod': 'getCities', 'Language': 'ru' }, { headers: { 'Content-Type': 'application/json;' }})
            .then(response => {
                commit('arrayAreaInformation', response.data);
                resolve()
            })
            .catch(function(reason) {
                console.log(reason);
            });
        })
    }
  },
  getters: {
    getNPAreas(state) {
       return state.np_areas;
    },
    getNPSettlements:(state) => (area, lang) => {
        let filtered = [];
        if (area != "") {
            if (lang.toUpperCase() == 'RU') {
                state.np_settlements.forEach(function(item,index) {
                    let obj = {};
                    if (item.Area == area) {
                        obj["key"] = item["Ref"];
                        obj["value"] = item["DescriptionRu"];
                        filtered.push(obj);
                    }
                });
            } else {
                state.np_settlements.forEach(function(item,index) {
                    let obj = {};
                    if (item.Area == area) {
                        obj[item["Ref"]] = item["Description"]
                        filtered.push(obj);
                    }
                });
            }
            return filtered;
        } else {
//            alert(JSON.stringify(state.np_settlements));
            //alert('3');
//            return state.np_settlements;
            return [];
        }
    },
  }
}
