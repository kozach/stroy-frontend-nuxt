import axios from 'axios'

export default {

  state: {
    projects: null,
    currentImage: null,
    cv_projects : null,
  },
  mutations: {
    arrayProjects : (state, payload) => {
      state.projects = payload;  
      for (let i = 0; i < state.projects.length; i++) {
         state.projects[i].is_current = false;
      }
    },
    setCurrentProject : (state, img) => {      
      state.currentImage = img;  
      for (let i = 0; i < state.projects.length; i++) {
        if(state.projects[i].uuid === img.uuid)
          state.projects[i].is_current = true;
        else
          state.projects[i].is_current = false;
      }
    },
    setProjectsVersion: (state, value) => {
        state.cv_projects = value;
    },
  },
  actions: {
    async getProjects( {commit, dispatch} ) {
        return  await new Promise((resolve) => {
            axios.get("https://test2.iti.dp.ua/api/project/")
                .then(response => {
                commit('arrayProjects', response.data);
                dispatch('setCurrent', response.data[0]);
                resolve()
            });
        })
    },
    setCurrent({commit}, img){
      commit('setCurrentProject', img);
    }
  },
  getters: {
    allProjects(state) {
       return state.projects
    },
    getProjectsVersion:(state) => () => {
        return state.cv_projects;
    },
    currentImage(state) {
       return state.currentImage;
    },
  }
}
