import axios from 'axios'

export default {

  state: {
    sort_options: {
        "ru": [
            'по наименованию',
            'по возрастанию цены',
            'по убыванию цены'
        ],
        "ua": [
            'за найменуванням',
            'за зростанням ціни',
            'за зменшенням ціни'
        ],
    },
    sort_type: 0,
  },
  mutations: {
    setSortType : (state, sort_type) => {
      state.sort_type = sort_type
    },
  },
  getters: {
    getSortType: (state) => {
       return state.sort_type;
    },
    getSortOptions: (state) => (lang) => {
        return state.sort_options[lang];
    }
  }
}
