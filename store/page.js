import axios from 'axios'
const debug =require('debug')('app:demo')

export default {

  state: {
    pages: null,
    cv_pages: null,
  },

  mutations: {
    arrayPages : (state, payload) => {
      state.pages = payload
    },
    setPagesVersion: (state, value) => {
        state.cv_pages = value;
    },
  },

  actions: {
      async getPages( {commit} ) {
          return await new Promise((resolve) => {
              axios.get("https://test2.iti.dp.ua/api/page/")
                .then(response => {
                  //debug(response.data);
                  commit('arrayPages', response.data);
                  resolve()
              });
          })
      },
  },
  getters: {
      allPages(state) {
          return state.pages;
      },
      getPage:(state) => (pageid,pagetype) => {
          let result = state.pages.filter(page => page.page_identifier==pageid).filter(page1 => page1.pagetype==pagetype);
          return result;
      },
      getPagesVersion:(state) => () => {
          return state.cv_pages;
      }
  }
}
