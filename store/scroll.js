import axios from 'axios'
const debug =require('debug')('app:demo')

export default {

  state: {
    scrollPosition: 0,
  },
  mutations: {    
    setScrollPosition: (state, value) => {
        state.scrollPosition = value;
    },
  },
  actions: {
      setScrollPositionValue( {commit}, value ) {
          commit('setScrollPosition', value)
      },
  },
  getters: {
      scrollPosition(state) {
        return state.scrollPosition;
      },      
  }
}


