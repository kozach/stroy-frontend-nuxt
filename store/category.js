import axios from 'axios'
const debug =require('debug')('app:demo')

export default {

  state: {
    categories: null,
    cv_categories: null,
  },
  mutations: {
    arrayCategories : (state, payload) => {
      state.categories = payload;
    },
    setCategoriesVersion: (state, value) => {
        state.cv_categories = value;
    },
  },
  actions: {
      async getCategories( {commit} ) {
          return await new Promise((resolve) => {
              axios.get("https://test2.iti.dp.ua/api/category/")
                .then(response => {
                  commit('arrayCategories', response.data);
                  resolve()
              });
          })
      },
  },
  getters: {
      allCategories:(state) => (lang) => {
          return state.categories.filter(item => item.language==lang);
      },
      getCategory: (state) => (catalias) => {
          let cats = state.categories.filter(item => item.alias == catalias);
          if (cats) return cats[0];
          else return null;
      },
      getCategoriesVersion:(state) => () => {
          return state.cv_categories;
      },
  }
}
