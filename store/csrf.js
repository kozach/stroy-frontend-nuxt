import axios from 'axios'
const debug =require('debug')('app:demo')

export default {

  state: {
    token: null,
  },

  mutations: {
    arrayToken : (state, payload) => {
      state.token = payload;
    },
  },

  actions: {
      async getCSRFToken( {commit} ) {
          return await new Promise((resolve) => {
              axios.get("https://test2.iti.dp.ua/api/csrf/")
                .then(response => {
                  commit('arrayToken', response.data.token);
                  resolve()
              });
          })
      },
  },
  getters: {
      getCSRFToken(state) {
          return state.token;
      }
  }
}
