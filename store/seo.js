import axios from 'axios'
const debug =require('debug')('app:demo')

export default {

  state: {
    params_sets: null,
    cv_seo: null,
  },

  mutations: {
    arrayParamsSets : (state, payload) => {
      state.params_sets = payload;
    },
    setSEOVersion: (state, value) => {
        state.cv_seo = value;
    },
  },

  actions: {
      async getSEOParams( {commit} ) {
          return await new Promise((resolve) => {
              axios.get("https://test2.iti.dp.ua/api/seo/")
                .then(response => {
                  commit('arrayParamsSets', response.data);
                  resolve()
              });
          })
      },
  },
  getters: {
      allParamsSets(state) {
          return state.params_sets;
      },
      getParamsSet:(state) => (setname) => {
          return state.params_sets.filter(item => item.params_set == setname);
      },
      getParamFromSet:(state) => (setname, paramname, lang) => {
          if (state.params_sets==null) return null;
          let params_set = state.params_sets.filter(item => item.params_set == setname)[0];
          //debug(params_set);
          if (!params_set) return null;
          let param = params_set.params.filter(item => (item.param==paramname && item.language==lang))[0];
          //debug(param);
          if (param) return param.value;
          return null;
      },
      getSEOVersion:(state) => () => {
          return state.cv_seo;
      },
  }
}
