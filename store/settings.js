import axios from 'axios'
const debug = require('debug')('app:demo')

export default {

  state: {
    settings: {
        curLang: null,
        defaultLang: "ru",
        languages: [ 'ru', 'ua' ],
    }
  },
  mutations: {
    setSettings : (state, payload) => {
      state.settings = payload;
    },
    setCurrentLanguage: (state, lang) => {
        state.settings.curLang = lang;
    },
  },
  actions: {
    async getSettings( {commit, dispatch} ) {
        return  await new Promise((resolve) => {
            axios.get("https://test2.iti.dp.ua/api/settings/")
                .then(response => {
                commit('arrayCurrentVersion', response.data);
                resolve()
            });
        })
    }
  },
  getters: {
    getLanguages(state) {
        return state.languages;
    },
    getDefaultLanguage(state) {
        return state.settings.defaultLang;
    },
    getCurrentLanguage(state) {
       return state.settings.curLang;
    },
  }
}
