import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import category from "./category"
import product from "./product"
import project from "./project"
import brand from "./brand"
import page from "./page"
import contact from "./contact"
import csrf from "./csrf"
import seo from "./seo"
import sort_select from "./sort_select"
import changes from "./changes"
import scroll from "./scroll"
import cart from "./cart"
import np from "./novapochta"
import settings from "./settings"

const store = () => new Vuex.Store({
    modules: {
        category,
        product,
        project,
        brand,
        page,
        contact,
        csrf,
        seo,
        sort_select,
        changes,
        scroll,
        cart,
        np,
        settings
    }
})

export default store

