import axios from 'axios'
const debug =require('debug')('app:demo')

export default {

  state: {
    contact: null,
    cv_contact: null,
  },

  mutations: {
    arrayContact : (state, payload) => {
      state.contact = payload;
    },
    setContactVersion:(state, value) => {
        state.cv_contact = value;
    },
  },
  actions: {
      async getContact( {commit} ) {
          return await new Promise((resolve) => {
              axios.get("https://test2.iti.dp.ua/api/contact/")
                .then(response => {
                  commit('arrayContact', response.data);
                  resolve()
              });
          })
      },
  },
  getters: {
      Contact:(state) => (lang) => {
          return state.contact.filter(item => item.language==lang);
      },
      getContactVersion:(state) => () => {
          return state.cv_contact;
      },
  }
}
