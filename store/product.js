import axios from 'axios'
    const debug = require('debug')('app:demo')

export default {

  state: {
    products: null,
    currentImage2: null,
    cv_products: null,
  },
  mutations: {
    arrayProducts : (state, payload) => {
      state.products = payload
      for (let i = 0; i < state.products.length; i++) {
          state.products[i].temp_count = 1;
          for (let j = 0; j<state.products[i].images.length; j++) {
              state.products[i].images[j].is_current = false;
          }
      }
//      debug(state.products);
    },
    setCurrentImage2: (state, image) => {
        state.currentImage2 = image
    },
    setProductsVersion: (state, value) => {
        state.cv_products = value;
    },
    setProductTempCount:(state, payload) => {
        //alert(payload.value);
        state.products.forEach(function(item,index) {
            if (payload.product.uuid == item.uuid) {
                item.temp_count = payload.value;
                return item.temp_count;
            }
        });
        state.products.forEach(function(item,index) {
            if (payload.product.uuid == item.uuid) {
//                alert(item.temp_count+"      "+item.uuid);
            }
        });
    },
  },

  actions: {
    async getProducts( {commit} ) {
        return  await new Promise((resolve) => {
            axios.get("https://test2.iti.dp.ua/api/product/")
                .then(response => {
            commit('arrayProducts', response.data);
                resolve()
            });
        })
    },
  },
  getters: {
    getProductTempCount: (state) => (product) => {
        let result;
        state.products.forEach(function(item,index) {
            if (product.uuid == item.uuid) {
                debug(item.temp_count);
                //alert(item.uuid+"     "+item.temp_count);
                result=item.temp_count;
                return;
            }
        });
//        alert(result);
        return result;
    },
    allProducts: (state) => (lang) => {
       return state.products.filter(item => item.language == lang);
    },
    getProductsForCategory: (state) => (catuuid,lang) => {
        return state.products.filter(product => ((product.category==catuuid) && (product.language == lang)));
    },
    getProduct: (state) => (catuuid, productalias, lang) => {
        let products = state.products.filter((product) => { return ( (product.category == catuuid) && (product.alias == productalias) && (product.language==lang)); });
        debug("+++++++++++    "+lang);
        debug("@@@@    "+products.length);

        if (products) { return products[0]; }
        else { return null; }
    },
    currentImage2(state) {
        //debug(state.currentImage2);
        return state.currentImage2;
    },
    getProductsVersion:(state) => () => {
        return state.cv_products;
    },
  }
}
