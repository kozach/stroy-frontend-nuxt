import axios from 'axios'

export default {

  state: {
    brands: null,
    cv_brands: null,
  },
  mutations: {
    arrayBrands : (state, payload) => {
      state.brands = payload
    },
    setBrandsVersion: (state,value) => {
        state.cv_brands = value
    },
  },

  actions: {
    async getBrands( {commit} ) {
        return  await new Promise((resolve) => {
            axios.get("https://test2.iti.dp.ua/api/brand/")
                .then(response => {
            commit('arrayBrands', response.data);
                resolve()
            });
        })
    },
  },
  getters: {
    allBrands(state) {
       return state.brands
    },
    getBrandsVersion:(state) => () => {
        return state.cv_brands;
    },
  }
}
