import axios from 'axios'
const { resolve } = require('path')
const debug = require('debug')('app:demo')

export default {
  server: {
    port: 19920,
    host: "0.0.0.0"
  },
  mode: 'universal',
  cache: false,
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { hid: 'keywords', name: 'keywords', content: ''},
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
//  hooks: hooks(this),
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '~assets/styles/index.less',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~plugins/vuelidate',
//    '~plugins/axios',
      { src: "@/plugins/vClickOutside",  ssr: false }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources',
    ['@nuxtjs/recaptcha', {
        hideBadge: false,
        siteKey: '6LehfPMUAAAAACn2dBtp8iHVYGf_Bp42uKylPv1g',
        version: 3,
    }],
    '@nuxtjs/router',
    '@nuxtjs/sitemap',
    ['nuxt-i18n',
    {
        detectBrowserLanguage: {
            useCookie: false,
        }
    }],
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
//    baseURL: "https://test2.iti.dp.ua/api"
  },
  /*
  ** Build configuration
  */
  styleResources: {
    less: './assets/styles/*.less'
  },
//  generate: {
//        axios.all([
//            axios.get("https://test2.iti.dp.ua/api/category/"),
//            axios.get("https://test2.iti.dp.ua/api/product/")
//        ])
//        .then(axios.spread(function (categories, products) {
//            let routes2 = categories.data.map((category) => {
//                return '/items_list/' + category.alias
//            });
//            let routes3 = products.data.map((product) => {
//                return '/items_list/' + product.category_alias + '/' + product.alias
//            });
//            callback(null, routes2.concat(routes3));
//        }), function(err) {
//            return next(err);
//        });
//    }
//  },
  sitemap: {
      hostname: 'https://test2.iti.dp.ua',
      routes: async () => {
          let routes = [{ "url": "/" }];
          let products;

          await axios.get("https://test2.iti.dp.ua/api/product/")
          .then(response => {
              products = response.data
          })
          await axios.get("https://test2.iti.dp.ua/api/category/")
          .then(response => {
              if (response.data) {
                  response.data.forEach((item) => { 
                      routes.push({"url": "/items_list/"+item.alias})
                      if (products) {
                          let filtered = products.filter(product => product.category==item.name);
                          if (filtered) {
                              filtered.forEach(function(product) {
                                  routes.push({ "url": "/items_list/"+item.alias+"/"+product.alias })
                              });
                          }
                      }
                  })
              }
          })
          return routes;
      }
  },
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
