import Vue from 'vue'
import Router from 'vue-router'
import Home from '~/pages/Home.vue'
import ItemsList from '~/pages/ItemsList.vue'
import Item from '~/pages/Item.vue'
import Cart from '~/pages/Cart.vue'
import Checkout from '~/pages/Checkout.vue'

Vue.use(Router)

export function createRouter() {
    return new Router({
        mode: 'history',
        base: process.env.BASE_URL,
        scrollBehavior () {
            return { x: 0, y: 0 }
        },
        routes: [
        {
            path: '/',
            name: 'main',
            component: Home
        },
        {
            path: '/items_list/:catalias',
            name: 'items_list',
            component: ItemsList
        },
        {
            path: '/items_list/:catalias/:productalias',
            name: 'product',
            component: Item
        },
        {
            path: '/cart',
            name: 'cart',
            component: Cart
        },
        {
            path: '/checkout',
            name: 'checkout',
            component: Checkout
        }]
    })
}
