export default function ({ $axios, store }) {
    $axios.onRequest( (config) => {
        if (store.state.token) {
            config.withCredentials = true
            config.headers.common['X-CSRFToken'] = store.state.token
        }
    })
}
